var express = require('express');
var router = express.Router();

var pool = require('../config/database');


//send msg to someone
router.post("/",function(req,res){
	var sender_id = req.body.sender_id;
	var receiver_id = req.body.receiver_id;
	var body = req.body.body;
	var attachment_path = req.body.attachment_path;
	
	var sql = "insert into message (sender_id,receiver_id,body,attachment_path) values (?,?,?,?)"
	var values =[sender_id,receiver_id,body,attachment_path];	
		pool.query(sql,values,function(err){
			if(err){
				console.log(err);
				res.json({
					status : false ,
					message : "sql err",
					data : null	
					
				});
			}else{
				res.json({
					status : true ,
					message : "done",
					data : null	
					
				});
			}
			
		})
	
	
});




// get messages from between user and specific users

router.get("/all",function(req,res){
	var user1 = req.query.user1;
	var user2 = req.query.user2;
	var sql = "select * from message where (sender_id = ? and receiver_id = ?) or (sender_id = ? and receiver_id = ?)";
	var values = [user1,user2,user2,user1];
	pool.query(sql,values,function(err,result){
	if(err){
		res.json({			
			status : false ,
			message : "sql err",
			data : null		
		});
		
	}else{
		
		res.json({			
			status : true ,
			message : "done",
			data : result			
		});
		
	}
	
	});
	
});












module.exports = router;