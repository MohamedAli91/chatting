var express = require('express');
var router = express.Router();

var pool = require('../config/database');





// Sign Up 

router.post('/',function(req,res,next){
	
	var username = req.body.username;
	var password = req.body.password;
	var sql = "insert into users (username,password) values(?,?)";
	var values = [username,password];
	pool.query(sql,values,function(err){
		if(err){
			console.log(err);
			res.json({
				status : false ,
				message : "This user already exists" ,
				data : null
				
			});
		}else{
			
			next();
		
		}
		
		
	});
	
	
},function(req,res){
	var username = req.body.username;
	var sql = "select id  from users where username = ?";
	pool.query(sql,username,function(err,result){
		if(err){
			console.log(err);
			res.json({
				status : false ,
				message : "sql err" ,
				data : null
				
			});
			
		}else{
			res.json({
				status : true ,
				message : "done" ,
				data : result[0].id
				
			});
			
		}	
		
	});
	
});

//////////////////////////////////////////////////////////////////

// sign in 

router.post('/login',function(req,res){
	
	var username = req.body.username;
	var password = req.body.password;
	var sql = "select id , password from users where username = ?";
	
	pool.query(sql,username,function(err,result){
		if(err){
			console.log(err);
			res.json({
				status : false ,
				message : "sql err" ,
				data : null
				
			});
		}else{
			
			if(password==result[0].password){
				res.json({
					status : true ,
					message : "done" ,
					data : result[0].id
				});
			}else{
				res.json({
					status : false ,
					message : "Wrong username or password" ,
					data : ""
				});
				
			}
			
			
			
		}
		
		
	});
	
});




/// get all other accounts 

router.get('/all/:userId',function(req,res){
	
	var id = req.params.userId ;
	
	var sql = "select  * from users where id != ? order by username";
	pool.query(sql,id,function(err,result){
				if(err){
			console.log(err);
			res.json({			
				status : false,
				data : null,
				message : err				
			});			
		}else{
			
			res.json({		
				status : true,
				data : result,
				message : "done"			
			});		
			
		}		
		
	});
	
});












module.exports = router;